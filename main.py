import datetime

from flask import Flask
from flask_restplus import Resource, Api, fields
import pandas_datareader as pdr
import requests
import json
from time import strptime

app = Flask(__name__)
api = Api(app)



@api.route('/Advice/<string:ticker>')
class BSH(Resource):
    def get(self, ticker):
        start_date = '2020-01-01'
        # start_date = datetime.datetime.now() - datetime.timedelta(1)

        df = pdr.get_data_yahoo(ticker, start_date)[['Close']]

        df['Date'] = df.index

        # 40 moving average
        df['40d mavg'] = df['Close'].rolling(window=40).mean()

        df['40d std'] = df['Close'].rolling(window=40).std()

        df['Upper Band'] = df['40d mavg'] + (df['40d std'] * 2)
        df['Lower Band'] = df['40d mavg'] - (df['40d std'] * 2)

        cols = ['40d mavg','Upper Band','Lower Band', 'Close']

        df_invest = df[cols]
        res_list = []

        for index,row in df_invest.iterrows():
            #print(type(index))
            # print(datetime.datetime.now())

            # strptime(str(index).replace('-','/'))
            time = str(index)[:10] + 'T00:00:00.884+00:00'
            print(time)


            if row['Close'] > row['Upper Band']:
                res = { 'Date' : str(index),
                         'Ans' : 'SELL',
                        'Price': row['Close']  }
                
                dictionary = {
                "dateCreated:": time,
                "stockTicker": ticker,
                "quantity": -100,
                "requestPrice": row['Close'],
                "tStatus": "CREATED"
                }
                json_object = json.dumps(dictionary, indent = 4)
                r = requests.post('http://localhost:8080//api/trades', data = json_object \
                    , headers={'Content-type':'application/json', 'Accept':'application/json'})

                res_list.append(res)
                res_list.append(dictionary)
            
            elif row['Close'] < row['Lower Band']:
                res = { 'Date' : str(index),
                         'Ans' : 'BUY',
                        'Price': row['Close']  }
                requests.post('http://localhost:8080//api/trades', data = res)

                
                dictionary = {
                "dateCreated:": time,
                "stockTicker": ticker,
                "quantity": 100,
                "requestPrice": row['Close'],
                "tStatus": "CREATED"
                }
                json_object = json.dumps(dictionary, indent = 4)
                r = requests.post('http://localhost:8080//api/trades', data = json_object \
                    , headers={'Content-type':'application/json', 'Accept':'application/json'})

                res_list.append(res)
                res_list.append(dictionary)
            else:
                res = { 'Date' : str(index),
                         'Ans' : 'HOLD',
                        'Price': row['Close'] }    

                res_list.append(res)
        return res_list



if __name__ == '__main__':
    app.run(debug=True)
